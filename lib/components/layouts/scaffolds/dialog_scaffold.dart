import 'package:flutter/material.dart';
import 'package:hagglex_test/styles/colors.dart';
import 'package:hagglex_test/utils/dimensions.dart';

class DialogScaffold extends StatelessWidget {
  final Widget child;
  final bool showClose;

  DialogScaffold({
    @required this.child,
    @required this.showClose,
  });

  @override
  Widget build(BuildContext context) {
    final scaler = AppScaleUtil(context);
    return Scaffold(
      backgroundColor: Colors.black12,
      body: Center(
        child: Container(
          margin: scaler.insets.only(
            top: .5,
            left: 7,
            right: 7,
          ),
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                if (showClose)
                Align(
                  alignment: Alignment.topRight,
                  child: InkWell(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      color: appPurple,
                      padding: scaler.insets.all(3),
                      child: Icon(
                        Icons.close,
                        color: appWhite,
                        size: scaler.fontSizer.sp(50),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: scaler.insets.only(
                    top: showClose ? 0 : 3,
                    bottom: 3,
                    left: 4,
                    right: 4,
                  ),
                  child: child,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
