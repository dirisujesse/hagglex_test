import 'package:flutter/material.dart';
import 'package:hagglex_test/components/fragments/indicators/app_spinner.dart';
import 'package:hagglex_test/components/fragments/spacers/app_sized_box.dart';
import 'package:hagglex_test/components/typography/app_text.dart';
import 'package:hagglex_test/styles/colors.dart';

enum MessageType {
  Error,
  Warning,
  Success,
  Info,
  Pending,
}

class DialogMessage extends StatelessWidget {
  final dynamic message;
  final MessageType messageType;
  final TextAlign textAlign;

  DialogMessage({
    @required this.message,
    this.messageType = MessageType.Info,
    this.textAlign = TextAlign.center,
  });

  String _parsedMessage() {
    if (message is String) {
      return message;
    } else if (message is Map || message is List) {
      final List<String> messageArr = message is Map
          ? message.values.map((it) => "$it").toList()
          : message.map((it) => "$it").toList();
      return messageArr.join("; ");
    } else {
      return "";
    }
  }

  Widget _messageIcon() {
    switch (messageType) {
      case MessageType.Error:
        return Icon(
          Icons.error_outline,
          size: 30,
          color: appRed,
        );
        break;
      case MessageType.Success:
        return Icon(
          Icons.sentiment_satisfied,
          size: 30,
          color: appYellow,
        );
        break;
      case MessageType.Warning:
        return Icon(
          Icons.warning,
          size: 30,
          color: appYellow,
        );
        break;
      case MessageType.Pending:
        return const AppSpinner();
        break;
      default:
        return Icon(
          Icons.notifications,
          size: 30,
          color: appPurple.withOpacity(.7),
        );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        _messageIcon(),
        const AppSizedBox(height: 1),
        Flexible(
          child: AppText(
            _parsedMessage(),
            textAlign: TextAlign.center,
          ),
        ),
      ],
    );
  }
}
