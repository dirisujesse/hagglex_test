import 'package:flutter/material.dart';
import 'package:hagglex_test/components/typography/app_text.dart';
import 'package:hagglex_test/styles/colors.dart';
import 'package:hagglex_test/styles/text_styles.dart';

enum SnackType { Default, Error, Info, Success, Warning }

showSnack({
  @required BuildContext context,
  @required String message,
}) {
  Scaffold.of(context).showSnackBar(
    SnackBar(
      content: AppText(
        message,
        textAlign: TextAlign.center,
        style: normalText.copyWith(
          color: appWhite,
        ),
      ),
      behavior: SnackBarBehavior.floating,
      backgroundColor: appYellow,
    ),
  );
}
