import 'dart:async';

import 'package:flutter/material.dart';
import 'package:hagglex_test/components/layouts/scaffolds/dialog_scaffold.dart';
import 'package:hagglex_test/components/typography/dialog_message.dart';

Future<T> formSubmitDialog<T>({
  @required BuildContext context,
  @required Future<T> future,
  String errorMessage =
      "An network failure occured and the request failed, please try again",
  String prompt = "Please wait as we submit your request",
  String successMessage,
}) async {
  ValueNotifier<bool> isResolved = ValueNotifier(false);
  final T result = await showDialog<T>(
    context: context,
    barrierDismissible: false,
    builder: (context) {
      return WillPopScope(
        child: ValueListenableBuilder(
          valueListenable: isResolved,
          builder: (context, bool val, _) {
            return DialogScaffold(
              child: FutureBuilder(
                future: future,
                builder: (context, AsyncSnapshot<T> res) {
                  if (res.hasData) {
                    WidgetsBinding.instance.addPostFrameCallback(
                      (_) {
                        Timer(
                          Duration(
                              milliseconds:
                                  successMessage == null ? 500 : 3000),
                          () {
                            Navigator.of(context).pop(res.data);
                          },
                        );
                      },
                    );
                    if (successMessage == null) {
                      return DialogMessage(
                        message: "",
                        messageType: MessageType.Pending,
                      );
                    }
                    return DialogMessage(
                      message: successMessage ?? "Success",
                      messageType: MessageType.Success,
                    );
                  }
                  if (res.hasError) {
                    WidgetsBinding.instance.addPostFrameCallback(
                      (_) {
                        Navigator.of(context).pop(res.error);
                      },
                    );

                    return DialogMessage(
                      message: errorMessage,
                      messageType: MessageType.Error,
                    );
                  }
                  return DialogMessage(
                    message: prompt,
                    messageType: MessageType.Pending,
                  );
                },
              ),
              showClose: val,
            );
          },
        ),
        onWillPop: () async => isResolved.value,
      );
    },
  );
  return result;
}
