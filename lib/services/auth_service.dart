import 'package:dio/dio.dart';
import 'package:hagglex_test/models/schemas/login_schema.dart';
import 'package:hagglex_test/models/schemas/register_schema.dart';
import 'package:hagglex_test/models/schemas/verify_schema.dart';
import 'package:hagglex_test/models/podos/service_response.dart';
import 'package:hive/hive.dart';

class AuthService {
  final db = Hive.box("user_data");
  final appDb = Hive.box("app_data");
  final http = Dio(
    BaseOptions(
      baseUrl: "https://hagglex-backend.herokuapp.com/graphql",
      receiveTimeout: 1000 * 120,
      connectTimeout: 1000 * 60,
    ),
  );

  Future<ServiceResponse> signup(RegisterSchma data) async {
    try {
      final payload = data.toJson();
      payload.removeWhere((key, value) => value == null);
      final req = await http.post(
        "",
        data: {
          "query": """
          mutation Register(\$data: CreateUserInput) {
            register(data: \$data) {
              user {
                id
                userProfile {
                  id
                  profilePicture
                }
                userRole {
                  id
                  description
                  role
                }
                active
              }
            }
          }
        """,
          "variables": {
            "data": payload,
          }
        },
      );
      return ServiceResponse(
        message: "Yaay your signup was succesful",
        isSuccessful: true,
        data: req.data["data"],
      );
    } on DioError catch (e) {
       String message = "";
      try {
        message = e.response.data["errors"][0]["message"];
      } catch (_) {
        message = "Ooops, signup failed because of an unexpected error";
      }
      throw ServiceResponse(
        message: message,
        isSuccessful: false,
        data: e.response.data,
      );
    }
  }

  Future<ServiceResponse> verify(VerifySchema data) async {
    try {
      final payload = data.toJson();
      payload.removeWhere((key, value) => value == null);
      final req = await http.post(
        "",
        data: {
          "query": """
          mutation VerifyUser(\$data: VerifyUserInput) {
            verifyUser(data: \$data) {
              token
              user {
                id
                userProfile {
                  id
                  profilePicture
                }
                userRole {
                  id
                  description
                  role
                }
                active
              }
            }
          }
        """,
          "variables": {
            "data": payload,
          }
        },
      );
      return ServiceResponse(
        message: "Yaay verification was succesful",
        isSuccessful: true,
        data: req.data["data"],
      );
    } on DioError catch (e) {
      String message = "";
      try {
        message = e.response.data["errors"][0]["message"];
      } catch (_) {
        message = "Ooops, verification failed because of an unexpected error";
      }
      throw ServiceResponse(
        message: message,
        isSuccessful: false,
        data: e.response.data,
      );
    }
  }

  Future<ServiceResponse> signin(LoginSchema data) async {
    try {
      final payload = data.toJson();
      payload.removeWhere((key, value) => value == null);
      final req = await http.post(
        "",
        data: {
          "query": """
          mutation Login(\$data: LoginInput!) {
            login(data: \$data) {
              token
              user {
                id
                userProfile {
                  id
                  profilePicture
                }
                userRole {
                  id
                  description
                  role
                }
                active
              }
            }
          }
        """,
          "variables": {
            "data": payload,
          }
        },
      );
      return ServiceResponse(
        message: "Yaay login was succesful",
        isSuccessful: true,
        data: req.data["data"],
      );
    } on DioError catch (e) {
      String message = "";
      try {
        message = e.response.data["errors"][0]["message"];
      } catch (_) {
        message = "Ooops, login failed because of an unexpected error";
      }
      throw ServiceResponse(
        message: message,
        isSuccessful: false,
        data: e.response.data,
      );
    }
  }

  set isPreviousUser(bool val) {
    appDb.put("isPreviousUser", val);
  }

  void logout() {
    db.delete("currentUser");
    db.put("isLoggedIn", false);
  }
}
