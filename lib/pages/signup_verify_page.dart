import 'package:flutter/material.dart';
import 'package:hagglex_test/components/fragments/buttons/app_back_button.dart';
import 'package:hagglex_test/components/fragments/buttons/app_button.dart';
import 'package:hagglex_test/components/fragments/spacers/app_sized_box.dart';
import 'package:hagglex_test/components/typography/app_text.dart';
import 'package:hagglex_test/models/schemas/register_schema.dart';
import 'package:hagglex_test/models/schemas/verify_schema.dart';
import 'package:hagglex_test/services/auth_service.dart';
import 'package:hagglex_test/styles/colors.dart';
import 'package:hagglex_test/styles/text_styles.dart';
import 'package:hagglex_test/utils/dimensions.dart';
import 'package:hagglex_test/utils/modals.dart';
import 'package:hagglex_test/utils/snacks.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class SignupVerifyPage extends StatefulWidget {
  final RegisterSchma data;
  SignupVerifyPage({
    @required this.data,
  });

  @override
  State<StatefulWidget> createState() {
    return _SignupVerifyPageState();
  }
}

class _SignupVerifyPageState extends State<SignupVerifyPage> {
  TextEditingController pin;
  GlobalKey<FormState> _form = GlobalKey();
  final service = AuthService();

  @override
  void initState() {
    super.initState();
    service.isPreviousUser = true;
    pin = TextEditingController();
  }

  _verify(BuildContext context) async {
    if (_form.currentState.validate()) {
      if (pin.text == null || pin.text.isEmpty) {
        showSnack(
          context: context,
          message: "Provide a pin",
        );
        return;
      }
      if (pin.text.length != 4) {
        showSnack(
          context: context,
          message: "Provide a pin 4 digits in length",
        );
        return;
      }
      if (int.tryParse(pin.text) == null) {
        showSnack(
          context: context,
          message: "The provided PIN must be digits",
        );
        return;
      }

      final data = await formSubmitDialog(
        context: context,
        future: service.verify(
          VerifySchema(
            code: int.tryParse(pin.text),
          ),
        ),
        prompt: "Please wait while we verify your code",
        successMessage: "Your registration was verified successfully",
      );
      if (data.isSuccessful) {
        Navigator.of(context).pushNamedAndRemoveUntil(
          "/signin",
          (route) => false,
        );
      } else {
        showSnack(
          context: context,
          message: data.message,
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final scaler = AppScaleUtil(context);
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Builder(
            builder: (context) {
              return Form(
                key: _form,
                child: SingleChildScrollView(
                  padding: scaler.insets.symmetric(
                    vertical: 2,
                    horizontal: 7,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          AppBackButton.plain(),
                          const SizedBox(),
                        ],
                      ),
                      const AppSizedBox(height: 4),
                      AppText(
                        "Verify Registration",
                        style: boldText.copyWith(
                          fontSize: 70,
                        ),
                      ),
                      const AppSizedBox(height: 4),
                      AppText(
                        "Please enter the code sent to ${widget.data.phonenumber}",
                        style: normalText.copyWith(fontSize: 40),
                      ),
                      const AppSizedBox(height: 4),
                      Padding(
                        padding: scaler.insets.symmetric(horizontal: 6),
                        child: PinCodeTextField(
                          length: 4,
                          onChanged: (val) {
                            pin.text = val;
                          },
                          onCompleted: (val) {
                            pin.text = val;
                            _verify(context);
                          },
                          shape: PinCodeFieldShape.underline,
                          selectedColor: appPurple,
                          activeColor: appYellow,
                          inactiveColor: appPurple.withOpacity(.5),
                          backgroundColor: appTransparent,
                          borderWidth: scaler.fontSizer.sp(5),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
      bottomNavigationBar: Builder(
        builder: (context) {
          return Container(
            padding: scaler.insets.symmetric(
              horizontal: 7,
            ),
            margin: scaler.insets.only(bottom: 4),
            constraints: BoxConstraints.tightFor(
              height: scaler.fontSizer.sp(140),
            ),
            child: AppButton(
              text: "Verify Code",
              onPressed: () {
                _verify(context);
              },
              isBold: true,
            ),
          );
        },
      ),
    );
  }
}
