import 'package:flutter/material.dart';
import 'package:hagglex_test/components/typography/app_text.dart';
import 'package:hagglex_test/styles/colors.dart';
import 'package:hagglex_test/styles/text_styles.dart';
import 'package:hagglex_test/utils/dimensions.dart';

class DashboardPage extends StatelessWidget {
  const DashboardPage();

  @override
  Widget build(BuildContext context) {
    final scaler = AppScaleUtil(context);
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).pushNamedAndRemoveUntil("/signin", (route) => false);
        },
        child: Icon(
          Icons.power_settings_new,
        ),
      ),
      body: Container(
        child: Center(
          child: AppText(
            "Dashboard Yaay!",
            style: boldText.copyWith(
              color: appYellow,
              fontSize: 60,
            ),
          ),
        ),
        padding: scaler.insets.symmetric(
          horizontal: 5,
        ),
      ),
    );
  }
}
