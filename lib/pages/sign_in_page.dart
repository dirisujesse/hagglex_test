import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hagglex_test/components/fragments/buttons/app_button.dart';
import 'package:hagglex_test/components/fragments/buttons/app_flat_button.dart';
import 'package:hagglex_test/components/fragments/spacers/app_sized_box.dart';
import 'package:hagglex_test/components/typography/app_text.dart';
import 'package:hagglex_test/models/schemas/login_schema.dart';
import 'package:hagglex_test/services/auth_service.dart';
import 'package:hagglex_test/styles/colors.dart';
import 'package:hagglex_test/styles/text_styles.dart';
import 'package:hagglex_test/utils/dimensions.dart';
import 'package:hagglex_test/utils/modals.dart';
import 'package:hagglex_test/utils/snacks.dart';
import 'package:hagglex_test/values/images.dart';
import 'package:hagglex_test/values/regex.dart';

class SigninPage extends StatefulWidget {
  const SigninPage();

  @override
  State<StatefulWidget> createState() {
    return _SigninPageState();
  }
}

class _SigninPageState extends State<SigninPage> {
  TextEditingController email;
  TextEditingController password;
  ValueNotifier<bool> _showPass;
  GlobalKey<FormState> _form = GlobalKey();
  final service = AuthService();

  @override
  void initState() {
    super.initState();
    _showPass = ValueNotifier(false);
    email = TextEditingController();
    password = TextEditingController();
  }

  _login(BuildContext context) async {
    if (_form.currentState.validate()) {
      final data = await formSubmitDialog(
        context: context,
        future: service.signin(
          LoginSchema(email: email.text, password: password.text),
        ),
        prompt: "Please wait as we log you in",
        successMessage: "Login successful"
      );
      if (data.isSuccessful) {
        Navigator.of(context).pushNamedAndRemoveUntil(
          "/dashboard",
          (route) => false,
        );
      } else {
        showSnack(
          context: context,
          message: data.message,
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final scaler = AppScaleUtil(context);

    return Scaffold(
      body: Builder(
        builder: (context) {
          return SafeArea(
            child: Container(
              child: Form(
                key: _form,
                child: SingleChildScrollView(
                  padding: scaler.insets.symmetric(
                    vertical: 2,
                    horizontal: 7,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      const AppSizedBox(height: 4),
                      AppSizedBox(
                        height: 30,
                        child: SvgPicture.asset(
                          AppSvgs.loginHeader,
                          fit: BoxFit.scaleDown,
                        ),
                      ),
                      const AppSizedBox(height: 4),
                      AppText(
                        "Welcome to\nHaggle_X trading",
                        style: boldText.copyWith(
                          fontSize: 70,
                        ),
                      ),
                      const AppSizedBox(height: 4),
                      AppText(
                        "Provide your credentials below to log into the app.",
                        style: normalText.copyWith(fontSize: 40),
                      ),
                      const AppSizedBox(height: 4),
                      TextFormField(
                        decoration: InputDecoration(hintText: "Email Address"),
                        controller: email,
                        keyboardType: TextInputType.emailAddress,
                        validator: (text) {
                          if (text == null || text.isEmpty) {
                            return "This field is required";
                          }
                          if (!mailRegEx.hasMatch(text)) {
                            return "Sorry the provided phone number is invalid";
                          }
                          return null;
                        },
                      ),
                      const AppSizedBox(height: 1.5),
                      ValueListenableBuilder(
                        valueListenable: _showPass,
                        builder: (context, bool val, _) {
                          return TextFormField(
                            validator: (text) {
                              if (text == null || text.isEmpty) {
                                return "This field is required";
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              hintText: "Password",
                              suffixIcon: InkWell(
                                onTap: () {
                                  _showPass.value = !val;
                                },
                                child: Icon(
                                  !val
                                      ? Icons.visibility
                                      : Icons.visibility_off,
                                  color: appBlack.withOpacity(.5),
                                ),
                              ),
                            ),
                            obscureText: !val,
                            controller: password,
                            keyboardType: TextInputType.visiblePassword,
                          );
                        },
                      ),
                      const AppSizedBox(height: 4),
                      AppButton(
                        text: "Signin",
                        onPressed: () {
                          _login(context);
                        },
                        isBold: true,
                      ),
                      const AppSizedBox(height: 1),
                      AppFlatButton(
                        onPressed: () {
                          Navigator.of(context).pushNamed("/signup");
                        },
                        child: RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(text: "Don't have an account? "),
                              TextSpan(
                                text: "Signup",
                                style: boldText.copyWith(
                                    fontSize: scaler.fontSizer.sp(35)),
                              ),
                            ],
                            style: normalText.copyWith(
                                color: appBlack,
                                fontSize: scaler.fontSizer.sp(35)),
                          ),
                          textAlign: TextAlign.center,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
