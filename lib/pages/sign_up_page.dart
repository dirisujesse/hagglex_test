import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hagglex_test/components/fragments/buttons/app_back_button.dart';
import 'package:hagglex_test/components/fragments/buttons/app_button.dart';
import 'package:hagglex_test/components/fragments/spacers/app_sized_box.dart';
import 'package:hagglex_test/components/typography/app_text.dart';
import 'package:hagglex_test/models/schemas/register_schema.dart';
import 'package:hagglex_test/services/auth_service.dart';
import 'package:hagglex_test/styles/colors.dart';
import 'package:hagglex_test/styles/text_styles.dart';
import 'package:hagglex_test/utils/dimensions.dart';
import 'package:hagglex_test/utils/modals.dart';
import 'package:hagglex_test/utils/snacks.dart';
import 'package:hagglex_test/values/regex.dart';

class SignupPage extends StatefulWidget {
  const SignupPage();

  @override
  State<StatefulWidget> createState() {
    return _SignupPageState();
  }
}

class _SignupPageState extends State<SignupPage> {
  TextEditingController name;
  TextEditingController telephone;
  TextEditingController password;
  TextEditingController email;
  ValueNotifier<bool> _showPass;
  GlobalKey<FormState> _form = GlobalKey();
  final service = AuthService();

  @override
  void initState() {
    super.initState();
    service.isPreviousUser = true;
    _showPass = ValueNotifier(false);
    name = TextEditingController();
    telephone = TextEditingController();
    email = TextEditingController();
    password = TextEditingController();
  }

  _signUp(BuildContext context) async {
    if (_form.currentState.validate()) {
      final payload = RegisterSchma(
        email: email.text,
        username: name.text,
        password: password.text,
        phonenumber: telephone.text,
        phoneNumberDetails: PhoneNumberDetails(
          phoneNumber: telephone.text,
          callingCode: "234",
          flag: "",
        ),
      );
      final data = await formSubmitDialog(
        context: context,
        future: service.signup(
          payload,
        ),
        prompt: "Wait as we register your account",
        successMessage: "Your account was registered successfully",
      );
      _form.currentState.reset();
      password.clear();
      if (data.isSuccessful) {
        Navigator.of(context).pushNamed(
          "/signup/verify",
          arguments: payload,
        );
      } else {
        showSnack(
          context: context,
          message: data.message,
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final scaler = AppScaleUtil(context);
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Form(
            key: _form,
            child: SingleChildScrollView(
              padding: scaler.insets.symmetric(
                vertical: 2,
                horizontal: 7,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      AppBackButton.plain(),
                      InkWell(
                        child: AppText(
                          "LOGIN",
                          style: normalText.copyWith(
                            color: appPurple,
                          ),
                        ),
                        onTap: () {
                          Navigator.of(context).pushNamed("/signin");
                        },
                      ),
                    ],
                  ),
                  const AppSizedBox(height: 4),
                  AppText(
                    "Signup",
                    style: boldText.copyWith(
                      fontSize: 70,
                    ),
                  ),
                  const AppSizedBox(height: 4),
                  AppText(
                    "Please ensure to provide the correct details below",
                    style: normalText.copyWith(fontSize: 40),
                  ),
                  const AppSizedBox(height: 4),
                  TextFormField(
                    decoration: InputDecoration(hintText: "Email"),
                    controller: email,
                    validator: (text) {
                      if (text == null || text.isEmpty) {
                        return "This field is required";
                      }
                      if (!mailRegEx.hasMatch(text)) {
                        return "Sorry the provided email is invalid";
                      }
                      return null;
                    },
                    keyboardType: TextInputType.emailAddress,
                  ),
                  const AppSizedBox(height: 1.5),
                  TextFormField(
                    decoration: InputDecoration(hintText: "Username"),
                    controller: name,
                    validator: (text) {
                      if (text == null || text.isEmpty) {
                        return "This field is required";
                      }
                      return null;
                    },
                    keyboardType: TextInputType.visiblePassword,
                  ),
                  const AppSizedBox(height: 1.5),
                  TextFormField(
                    decoration: InputDecoration(hintText: "Phone number"),
                    controller: telephone,
                    keyboardType: TextInputType.phone,
                    inputFormatters: [
                      LengthLimitingTextInputFormatter(11),
                    ],
                    validator: (text) {
                      if (text == null || text.isEmpty) {
                        return "This field is required";
                      }
                      if (!phoneRegEx.hasMatch(text)) {
                        return "Sorry the provided phone number is invalid";
                      }
                      return null;
                    },
                  ),
                  const AppSizedBox(height: 1.5),
                  ValueListenableBuilder(
                    valueListenable: _showPass,
                    builder: (context, bool val, _) {
                      return TextFormField(
                        validator: (text) {
                          if (text == null || text.isEmpty) {
                            return "This field is required";
                          }
                          if (!passRegEx.hasMatch(text)) {
                            return "Sorry the provided password is not strong enough, passwords should have at least one capital letter";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          hintText: "Password",
                          suffixIcon: InkWell(
                            onTap: () {
                              _showPass.value = !val;
                            },
                            child: Icon(
                              !val ? Icons.visibility : Icons.visibility_off,
                              color: appBlack.withOpacity(.5),
                            ),
                          ),
                        ),
                        obscureText: !val,
                        controller: password,
                        keyboardType: TextInputType.visiblePassword,
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      bottomNavigationBar: Builder(
        builder: (context) {
          return Container(
            padding: scaler.insets.symmetric(
              horizontal: 7,
            ),
            margin: scaler.insets.only(bottom: 4),
            constraints: BoxConstraints.tightFor(
              height: scaler.fontSizer.sp(140),
            ),
            child: AppButton(
              text: "Signup",
              onPressed: () {
                _signUp(context);
              },
              isBold: true,
            ),
          );
        },
      ),
    );
  }
}
