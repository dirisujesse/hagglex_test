import 'package:flutter/material.dart';

class RegisterSchma {
  String email;
  String username;
  String password;
  String phonenumber;
  String referralCode;
  PhoneNumberDetails phoneNumberDetails;

  RegisterSchma({
    @required this.email,
    @required this.username,
    @required this.password,
    @required this.phonenumber,
    this.referralCode,
    this.phoneNumberDetails,
  });

  RegisterSchma.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    username = json['username'];
    password = json['password'];
    phonenumber = json['phonenumber'];
    referralCode = json['referralCode'];
    phoneNumberDetails = json['phoneNumberDetails'] != null
        ? new PhoneNumberDetails.fromJson(json['phoneNumberDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['username'] = this.username;
    data['password'] = this.password;
    data['phonenumber'] = this.phonenumber;
    data['referralCode'] = this.referralCode;
    if (this.phoneNumberDetails != null) {
      data['phoneNumberDetails'] = this.phoneNumberDetails.toJson();
    }
    return data;
  }
}

class PhoneNumberDetails {
  String phoneNumber;
  String callingCode;
  String flag;

  PhoneNumberDetails({
    this.phoneNumber,
    this.callingCode,
    this.flag,
  });

  PhoneNumberDetails.fromJson(Map<String, dynamic> json) {
    phoneNumber = json['phoneNumber'];
    callingCode = json['callingCode'];
    flag = json['flag'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['phoneNumber'] = this.phoneNumber;
    data['callingCode'] = this.callingCode;
    data['flag'] = this.flag;
    return data;
  }
}
