import 'package:flutter/material.dart';

class LoginSchema {
  String email;
  String password;

  LoginSchema({@required this.email, @required this.password});

  LoginSchema.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    password = json['password'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['password'] = this.password;
    return data;
  }
}
