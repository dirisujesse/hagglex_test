class ServiceResponse {
  Map data;
  String message;
  bool isSuccessful;

  ServiceResponse({
    this.message,
    this.data,
    this.isSuccessful,
  });
}