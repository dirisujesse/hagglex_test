import 'package:flutter/material.dart'
    show BuildContext, Widget, RouteSettings, MaterialPageRoute, Route;
import 'package:hagglex_test/pages/dashboard_page.dart';
import 'package:hagglex_test/pages/sign_in_page.dart';
import 'package:hagglex_test/pages/sign_up_page.dart';
import 'package:hagglex_test/pages/signup_verify_page.dart';

final Map<String, Widget Function(BuildContext)> staticRoutes = {
  "/dashboard": (BuildContext context) => const DashboardPage(),
  "/signup": (BuildContext context) => const SignupPage(),
  "/signin": (BuildContext context) => const SigninPage(),
};

Route<dynamic> dynamicRoutes(RouteSettings settings) {
  switch (settings.name) {
    case "/signup/verify":
      return MaterialPageRoute(
        builder: (context) {
          return SignupVerifyPage(
            data: settings.arguments,
          );
        },
      );
    default:
      return MaterialPageRoute(
        builder: (context) {
          return const SignupPage();
        },
      );
  }
}
